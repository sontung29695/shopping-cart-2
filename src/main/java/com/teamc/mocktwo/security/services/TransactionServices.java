package com.teamc.mocktwo.security.services;

import com.teamc.mocktwo.dto.UserDto;
import com.teamc.mocktwo.models.Order;
import com.teamc.mocktwo.models.Transaction;
import com.teamc.mocktwo.models.User;
import com.teamc.mocktwo.repository.*;
import com.teamc.mocktwo.security.jwt.JwtUtils;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TransactionServices {
    @Autowired
    AuthenticationManager authenticationManager;
    
    @Autowired
    PasswordEncoder encoder;
    
    @Autowired
    JwtUtils jwtUtils;
    
    @Autowired
    @Qualifier("UserRepository")
    UserRepository userRepository;
    
    @Autowired
    @Qualifier("CartRepository")
    CartRepository cartRepository;
    
    @Autowired
    @Qualifier("Cart_ItemRepository")
    Cart_ItemRepository cartItemRepository;
    
    @Autowired
    @Qualifier("RoleRepository")
    RoleRepository roleRepository;
    
    @Autowired
    @Qualifier("Audit_LogRepository")
    Audit_LogRepository audit_logRepository;
    
    @Autowired
    @Qualifier("ProductReviewRepository")
    ProductReviewRepository productReviewRepository;
    
    @Autowired
    @Qualifier("TransactionRepository")
    TransactionRepository transactionRepository;
    
    @Autowired
    @Qualifier("OrderRepository")
    OrderRepository orderRepository;
    
    
    @Autowired
    @Qualifier("Order_ItemRepository")
    Order_ItemRepository orderItemRepository;
    
    @Autowired
    UserServices userServices;
    
    
    @Autowired
    @Qualifier("ModelMapper")
    private ModelMapper modelMaper;
    private static final Logger auditTechLog = Logger.getLogger(TransactionServices.class);
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public void addTransaction(Order order, String code) {
        User user = userServices.getUser();
        Order od = orderRepository.findById(order.getOrderId()).get();
        Transaction transaction = new Transaction(code, od.getAmountTotal(), user,od);
        transactionRepository.save(transaction);
        auditTechLog.info("Save product: "+ transaction.getCode());
    }
//    public void addTransaction(Order order, String code) {
//        User user = userServices.getUser();
//        Order od = orderRepository.findById(order.getOrderId()).get();
//        Transaction transaction = new Transaction(code, od.getAmountTotal());
//        transactionRepository.save(transaction);
//        transactionRepository.insertUserIdOrderId(od.getOrderId(), user.getId(),code);
//    }
}
