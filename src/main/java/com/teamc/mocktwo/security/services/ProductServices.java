package com.teamc.mocktwo.security.services;

import com.teamc.mocktwo.dto.ProductDto;
import com.teamc.mocktwo.dto.UserDto;
import com.teamc.mocktwo.exception.MyException;
import com.teamc.mocktwo.models.Product;
import com.teamc.mocktwo.models.User;
import com.teamc.mocktwo.repository.Cart_ItemRepository;
import com.teamc.mocktwo.repository.Order_ItemRepository;
import com.teamc.mocktwo.repository.ProductRepository;
import com.teamc.mocktwo.repository.ProductReviewRepository;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServices {
    @Autowired
    private ProductRepository productRepository;
    
    @Autowired
    private Order_ItemRepository orderItemRepository;
    
    @Autowired
    private Cart_ItemRepository cartItemRepository;
    
    @Autowired
    private ProductReviewRepository productReviewRepository;
    
    @Autowired
    @Qualifier("ModelMapper")
    private ModelMapper modelMaper;
    private static final Logger auditTechLog = Logger.getLogger(ProductServices.class);
    
    public List<ProductDto> listProducts(){
        List<Product> list = productRepository.findAll();
        List<ProductDto> listDTO = new ArrayList<>();
        for (Product l:list) {
            listDTO.add(modelMaper.map(l, ProductDto.class));
        }
        return listDTO;
    }
    public void addProduct(Product product){
        if(!productRepository.existsByName(product.getName())){
            productRepository.save(product);
            auditTechLog.info("Save product: "+ product.getName());
        }
    }
    public void updateProduct(Product product){
        if(!productRepository.existsByName(product.getName())) {
            Product p = productRepository.findById(product.getId()).get();
            p = product;
            productRepository.save(p);
            auditTechLog.info("Update product: "+ product.getName());
        }else{
            System.out.println("Không tìm thấy sản phẩm cần sửa!");
        }
    }
    public void changeQuantityProduct(Product product, Long number) {
        Product p = productRepository.findById(product.getId()).get();
        if(p.getQuantity() >= number){
            p.setQuantity(p.getQuantity()-number);
            auditTechLog.info("Change quantity product: "+ product.getName());
        }else{
            System.out.println("Số sản phẩm còn lại ít hơn số cần giảm!");
        }
        productRepository.save(p);
    }
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public void deleteProduct(Product product){
        if(productRepository.existsByName(product.getName())){
            productReviewRepository.deleteByProduct_id(product.getId());
            auditTechLog.info("Delete product review: "+ product.getName());
            cartItemRepository.deleteByProduct_id(product.getId());
            auditTechLog.info("Delete product in cart: "+ product.getName());
            orderItemRepository.deleteByProduct_id(product.getId());
            auditTechLog.info("Delete product in order: "+ product.getName());
            productRepository.deleteById(product.getId());
            auditTechLog.info("Delete product: "+ product.getName());
        }
    }
}
