package com.teamc.mocktwo.security.services;

import com.teamc.mocktwo.DemoApplication;
import com.teamc.mocktwo.models.*;
import com.teamc.mocktwo.repository.Audit_LogRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.*;
@Component
public class AuditListener<T> { // keywords phần log là AuditListener, log4j.Logger appender jpa
    private static Audit_LogRepository auditLogRepository;
    private static final Logger auditTechLog = Logger.getLogger(AuditListener.class);
    // tao 1 static Audit_LogRepository
    @Autowired
    public void setAuditLogRepository (Audit_LogRepository auditLogRepository) {
        AuditListener.auditLogRepository = auditLogRepository;
    }
    @PrePersist
    private void beforeAnyPersist(T t) {
        AuditBusinessLog auditLog = null;
        if(t instanceof User){
            auditLog = new AuditBusinessLog("Persist", "Persist user: " + ((User) t).getUsername());
//            auditTechLog.info("Save user!");
        }
        if(t instanceof Product){
            auditLog = new AuditBusinessLog("Persist", "Persist product: " + ((Product) t).getName());
//            auditTechLog.info("Save product!");
        }
        if(t instanceof Cart){
            auditLog = new AuditBusinessLog("Persist", "Persist cart: "+((Cart) t).getUser().getUsername());
//            auditTechLog.info("Save cart!");
        }
        if(t instanceof Cart_Item){
            auditLog = new AuditBusinessLog("Persist", "Persist cartItem: "+((Cart_Item) t).getCartItemId());
//            auditTechLog.info("Save cartItem!");
        }
        if(t instanceof Order){
            auditLog = new AuditBusinessLog("Persist", "Persist order: "+ ((Order) t).getOrderId());
//            auditTechLog.info("Save order!");
        }
        if(t instanceof Order_Item){
            auditLog = new AuditBusinessLog("Persist", "Persist Order_Item: "+((Order_Item) t).getOrderItemId());
//            auditTechLog.info("Save Order_Item!");
        }
        if(t instanceof Transaction){
            auditLog = new AuditBusinessLog("UPDATE", "Persist Transaction: "+((Transaction) t).getId());
//            auditTechLog.info("Save Transaction!");
        }
        if(t instanceof ProductReview){
            auditLog = new AuditBusinessLog("Persist", "Persist ProductReview: "+((ProductReview) t).getId());
//            auditTechLog.info("Save ProductReview!");
        }
    
        auditLogRepository.save(auditLog);
    }
    @PreUpdate
    private void beforeAnyUpdate(T t) {
        AuditBusinessLog auditLog = null;
        if(t instanceof User){
            auditLog = new AuditBusinessLog("Update", "Update user: " + ((User) t).getUsername());
//            auditTechLog.info("Update user!");
        }
        if(t instanceof Product){
            auditLog = new AuditBusinessLog("Update", "Update product: " + ((Product) t).getName());
//            auditTechLog.info("Update product!");
        }
        if(t instanceof Cart){
            auditLog = new AuditBusinessLog("Update", "Update cart: "+((Cart) t).getUser().getUsername());
//            auditTechLog.info("Update cart!");
        }
        if(t instanceof Cart_Item){
            auditLog = new AuditBusinessLog("Update", "Update cartItem: "+((Cart_Item) t).toString());
//            auditTechLog.info("Update cartItem!");
        }
        if(t instanceof Order){
            auditLog = new AuditBusinessLog("Update", "Update order: "+ ((Order) t).getOrderId());
//            auditTechLog.info("Update order!");
        }
        if(t instanceof Order_Item){
            auditLog = new AuditBusinessLog("Update", "Update Order_Item: "+((Order_Item) t).getOrderItemId());
//            auditTechLog.info("Update Order_Item!");
        }
        if(t instanceof Transaction){
            auditLog = new AuditBusinessLog("UPDATE", "Update Transaction: "+((Transaction) t).getId());
//            auditTechLog.info("Update Transaction!");
        }
        if(t instanceof ProductReview){
            auditLog = new AuditBusinessLog("Update", "Update ProductReview: "+((ProductReview) t).getId());
//            auditTechLog.info("Update ProductReview!");
        }
    
        auditLogRepository.save(auditLog);
    }
    @PreRemove
    private void beforeAnyRemove(T t) {
        AuditBusinessLog auditLog = null;
        if(t instanceof User){
            auditLog = new AuditBusinessLog("Remove", "Remove user: " + ((User) t).getUsername());
//            auditTechLog.info("Remove user!");
        }
        if(t instanceof Product){
            auditLog = new AuditBusinessLog("Remove", "Remove product: " + ((Product) t).getName());
//            auditTechLog.info("Remove product!");
        }
        if(t instanceof Cart){
            auditLog = new AuditBusinessLog("Remove", "Remove cart: "+((Cart) t).getUser().getUsername());
//            auditTechLog.info("Remove cart!");
        }
        if(t instanceof Cart_Item){
            auditLog = new AuditBusinessLog("Remove", "Remove cartItem: "+((Cart_Item) t).getCartItemId());
//            auditTechLog.info("Remove cartItem!");
        }
        if(t instanceof Order){
            auditLog = new AuditBusinessLog("Remove", "Remove order: "+ ((Order) t).getOrderId());
//            auditTechLog.info("Remove order!");
        }
        if(t instanceof Order_Item){
            auditLog = new AuditBusinessLog("Remove", "Remove Order_Item: "+((Order_Item) t).getOrderItemId());
//            auditTechLog.info("Remove Order_Item!");
        }
        if(t instanceof Transaction){
            auditLog = new AuditBusinessLog("Remove", "Remove Transaction: "+((Transaction) t).getId());
//            auditTechLog.info("Remove Transaction!");
        }
        if(t instanceof ProductReview){
            auditLog = new AuditBusinessLog("Remove", "Remove ProductReview: "+((ProductReview) t).getId());
//            auditTechLog.info("Remove ProductReview!");
        }
    
        auditLogRepository.save(auditLog);
    }
    @PostPersist
    private void afterAnyPersist(T t) {
    
    
    }
    @PostLoad
    private void afterLoad(T t) {
    
    }
}
