package com.teamc.mocktwo.security.services;

import com.teamc.mocktwo.models.Product;
import com.teamc.mocktwo.models.ProductReview;
import com.teamc.mocktwo.models.User;
import com.teamc.mocktwo.repository.ProductRepository;
import com.teamc.mocktwo.repository.ProductReviewRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Service
public class ProductReviewServices {
    
    @Autowired
    UserServices userServices;
    
    @Autowired
    private EntityManager entityManager;
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    @Qualifier("ProductRepository")
    ProductRepository productRepository;
    
    @Autowired
    @Qualifier("ProductReviewRepository")
    ProductReviewRepository productReviewRepository;
    
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public void addReview(Long productId, String productReview){
        User user = userServices.getUser();
        Product product = productRepository.findById(productId).get();
        ProductReview productReview1 = new ProductReview(user, product, productReview);
        productReviewRepository.save(productReview1);
//        entityManager.flush();
//        entityManager.clear();
    }

    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public void deleteReview(Long productReviewId){
        User user = userServices.getUser();
        ProductReview productReview = productReviewRepository.findById(productReviewId).get();
        if(user.getUsername().equals(productReview.getUser().getUsername())){
            productReviewRepository.deleteById(productReview.getId());
//            entityManager.flush();
//            entityManager.clear();
        }else{
            System.out.println("Bạn không có quyền xóa!");
        }
        
    }
}
