package com.teamc.mocktwo.security.services;

import com.teamc.mocktwo.dto.UserDto;
import com.teamc.mocktwo.models.Cart;
import com.teamc.mocktwo.models.Cart_Item;
import com.teamc.mocktwo.models.Product;
import com.teamc.mocktwo.models.User;
import com.teamc.mocktwo.repository.CartRepository;
import com.teamc.mocktwo.repository.Cart_ItemRepository;
import com.teamc.mocktwo.repository.ProductRepository;
import com.teamc.mocktwo.repository.UserRepository;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class CartServices {
    @Autowired
    private ProductRepository productRepository;
    
    @Autowired
    private CartRepository cartRepository;
    
    @Autowired
    private Cart_ItemRepository cartItemRepository;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private UserServices userServices;
    
    @Autowired
    @Qualifier("ModelMapper")
    private ModelMapper modelMaper;
    
    
    public void addToCart(Product product){
        User user = userServices.getUser();
        Product product1 = productRepository.findById(product.getId()).get();
        Set<Cart_Item> cartItemSet = user.getCart().getCartItem();
        Cart cart = user.getCart();
        Cart_Item cartItem = null;
        Boolean exists = false;
        for(Cart_Item ci : cartItemSet){
            if(ci.getProduct().getId() == product.getId()){
                exists = true;
                cartItem = ci;
                break;
            }
        }
        // Kiểm tra tồn tại cart Item
        if(!exists){
            //nếu sản phẩm chưa tồn tại trong giỏ thi tạo sản phẩm và thêm vào, tính giá của giỏ và sản phẩm đó
           cartItem = new Cart_Item(user.getCart(), product1, product.getQuantity(), product1.getPrice()*product.getQuantity());
            product1.addCartItem(cartItem);
            user.getCart().addCartItem(cartItem);
        }else{
            if(cartItem.getQuantityProduct()+ product.getQuantity()<= product1.getQuantity()){
                cartItem.setQuantityProduct(cartItem.getQuantityProduct()+ product.getQuantity());
                cartItem.setPriceTotal(cartItem.getPriceTotal()+product1.getPrice()* product.getQuantity());
            }else{
                System.out.println("Số lượng không đủ!");
            }
            
        }
        cartItemRepository.save(cartItem);
        userRepository.save(user);
        productRepository.save(product1);
    }
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public void deleteFromCart(Product product){
        System.out.println("Kiểm tra người dùng xóa sản phẩm vào giỏ!");
        User user = userServices.getUser();
        Cart_Item cart_Item = cartItemRepository.findByProduct_id(product.getId());
        Cart cart = user.getCart();
        Product product1 = productRepository.findById(product.getId()).get();
        if(cart_Item==null){
            System.out.println("Không tìm thấy sản phẩm trong giỏ!");
        }else{
            if(product.getQuantity()>=cart_Item.getQuantityProduct()){
                cartItemRepository.delete(cart_Item);
                disableProductInCart(product1);
            }else{
                //Không cần save lai vì có chú thích Transaction - tự động commit khi hết phương thức
                cart_Item.setQuantityProduct(cart_Item.getQuantityProduct()- product.getQuantity());
                cart_Item.setPriceTotal(cart_Item.getPriceTotal()-product1.getPrice()* product.getQuantity());
                if(cart_Item.getEnable()){
                    cart.setPriceItemEnable(cart.getPriceItemEnable()-product1.getPrice()* product.getQuantity());
                }
            }
        }
    }
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public void enableProductInCart(Product product){
        User user = userServices.getUser();
        Cart cart = user.getCart();
        Cart_Item cart_Item = cartItemRepository.findByProduct_id(product.getId());
        if(cart_Item==null){
            System.out.println("Không tìm thấy sản phẩm trong giỏ!");
        }else{
            cart_Item.setEnable(true);
            cart.setPriceItemEnable(cart.getPriceItemEnable()+cart_Item.getPriceTotal());
        }
    }
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public void disableProductInCart(Product product){
        User user = userServices.getUser();
        Cart cart = user.getCart();
        Cart_Item cart_Item = cartItemRepository.findByProduct_id(product.getId());
        if(cart_Item==null){
            System.out.println("Không tìm thấy sản phẩm trong giỏ!");
        }else{
            cart_Item.setEnable(false);
            cart.setPriceItemEnable(cart.getPriceItemEnable()-cart_Item.getPriceTotal());
        }
    }
}