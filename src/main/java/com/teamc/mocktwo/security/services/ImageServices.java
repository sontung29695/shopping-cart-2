package com.teamc.mocktwo.security.services;

import com.teamc.mocktwo.models.Image;
import com.teamc.mocktwo.models.User;
import com.teamc.mocktwo.repository.FileSystemRepository;
import com.teamc.mocktwo.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.nio.file.*;

@Service
public class ImageServices {
    @Autowired
    private ImageRepository imageRepository;
    
    @Autowired
    private FileSystemRepository fileSystemRepository;
    
    @Autowired
    private UserServices userServices;
    
    public void uploadImage(MultipartFile multipartImage) throws IOException {
        User user = userServices.getUser();
        Image dbImage = new Image();
        dbImage.setName(multipartImage.getOriginalFilename());
        dbImage.setContent(multipartImage.getBytes());
        dbImage.setUser(user);
        imageRepository.save(dbImage);
    }
    public Resource downloadImage(Long imageId) {
        byte[] image = imageRepository.findById(imageId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND))
                .getContent();
    
        return new ByteArrayResource(image);
    }
    FileSystemResource findInFileSystem(String location) {
        try {
            return new FileSystemResource(Paths.get(location));
        } catch (Exception e) {
            // Handle access or file not found problems.
            throw new RuntimeException();
        }
    }
    public void uploadImageSys(MultipartFile multipartImage) throws Exception {
        User user = userServices.getUser();
        String uploadDir = "user-photos/" + user.getId();
        FileUploadUtil.saveFile(uploadDir, multipartImage.getOriginalFilename(), multipartImage);
    }
    public void deleteImageSys(String pathString) throws Exception {
        Path path = Paths.get(pathString);
    
        try {
            // Delete file or directory
            Files.delete(path);
            System.out.println("File or directory deleted successfully");
        } catch (NoSuchFileException ex) {
            System.out.printf("No such file or directory: %s\n", path);
        } catch (DirectoryNotEmptyException ex) {
            System.out.printf("Directory %s is not empty\n", path);
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }
    public void deleteImage(Image image) throws Exception {
        User user = userServices.getUser();
        if(user.getId() == image.getUser().getId()){
            imageRepository.delete(imageRepository.findById(image.getImageId()).get());
        }
    }
}
