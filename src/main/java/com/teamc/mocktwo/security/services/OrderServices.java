package com.teamc.mocktwo.security.services;

import com.teamc.mocktwo.dto.OrderDto;
import com.teamc.mocktwo.models.*;
import com.teamc.mocktwo.repository.*;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


@Service
public class OrderServices {
    @Autowired
    private ProductRepository productRepository;
    
    @Autowired
    private CartRepository cartRepository;
    
    @Autowired
    private Cart_ItemRepository cartItemRepository;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private UserServices userServices;
    
    @Autowired
    private OrderRepository orderRepository;
    
    @Autowired
    private Order_ItemRepository orderItemRepository;
    
    @Autowired
    private TransactionRepository transactionRepository;
    
    @Autowired
    private CartServices cartServices;
    
    @Autowired
    @Qualifier("ModelMapper")
    private ModelMapper modelMaper;
    
    private static final Logger auditTechLog = Logger.getLogger(OrderServices.class);
    public List<OrderDto> listOrders() {
        Iterable<Order> list = orderRepository.findAll();
        List<OrderDto> listDTO = new ArrayList<OrderDto>();
        for (Order o:list) {
            listDTO.add(modelMaper.map(o, OrderDto.class));
        }
        return listDTO;
    }
    
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public void addOrder() {
        User user = userServices.getUser();
        Order order = new Order(user);
        orderRepository.save(order);
        user.addOrder(order);
        Iterable<Cart_Item> listCartItem = cartItemRepository.findByEnable();
        if (listCartItem != null){
            for (Cart_Item ci : listCartItem) {
                Order_Item orderItem = new Order_Item(order, ci.getProduct(), ci.getQuantityProduct());
                orderItemRepository.save(orderItem);
                ci.getProduct().setQuantity(ci.getProduct().getQuantity()-ci.getQuantityProduct());
                order.addOrderItem(orderItem);
                order.setAmountTotal(order.getAmountTotal()+orderItem.getQuantityProduct()*orderItem.getProduct().getPrice());
            }
            int count = user.getCart().getCartItem().size();
            Set<Cart_Item> listCI = new LinkedHashSet<>();
            for(Cart_Item ci : user.getCart().getCartItem()){
              if(ci.getEnable()){
                  listCI.add(ci);
              }
            }
            auditTechLog.info("Add Order: "+ order.getOrderId());
            user.getCart().getCartItem().removeAll(listCI);
            user.getCart().setPriceItemEnable((long)0);
//            userRepository.save(user);
        }
    }
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public void deleteOrder(Order order) {
        transactionRepository.deleteByOrderId(order.getOrderId());
        auditTechLog.info("RollBack Transaction of Order: "+ order.getOrderId());
        orderItemRepository.deleteByOrderId(order.getOrderId());
        orderRepository.deleteByOrder_id(order.getOrderId());
        auditTechLog.info("RollBack Order: "+ order.getOrderId());
    }
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public void rollBackCart(Order order) {
        User user = userServices.getUser();
        Product prd = null;
        for(Order od : user.getOrders()){
            if(od.getOrderId() == order.getOrderId()){
                order = od;
                break;
            }
        }
        for(Order_Item oi : order.getOrder_items()){
            prd = new Product(oi.getProduct().getId(), oi.getProduct().getPrice(), oi.getQuantityProduct());
            cartServices.addToCart(prd);
            oi.getProduct().setQuantity(oi.getProduct().getQuantity()+oi.getQuantityProduct());
        }
        auditTechLog.info("RollBack Order: "+ order.getOrderId());
    }
}