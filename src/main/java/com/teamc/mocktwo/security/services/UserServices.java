package com.teamc.mocktwo.security.services;

import com.teamc.mocktwo.DemoApplication;
import com.teamc.mocktwo.dto.UserDto;
import com.teamc.mocktwo.exception.UserException;
import com.teamc.mocktwo.models.*;
import com.teamc.mocktwo.repository.*;
import com.teamc.mocktwo.security.jwt.JwtUtils;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class UserServices {
    @Autowired
    AuthenticationManager authenticationManager;
    
    @Autowired
    PasswordEncoder encoder;
    
    @Autowired
    JwtUtils jwtUtils;
    
    @Autowired
    @Qualifier("UserRepository")
    UserRepository userRepository;
    
    @Autowired
    @Qualifier("CartRepository")
    CartRepository cartRepository;
    
    @Autowired
    @Qualifier("Cart_ItemRepository")
    Cart_ItemRepository cartItemRepository;
    
    @Autowired
    @Qualifier("RoleRepository")
    RoleRepository roleRepository;
    
    @Autowired
    @Qualifier("Audit_LogRepository")
    Audit_LogRepository audit_logRepository;
    
    @Autowired
    @Qualifier("ProductReviewRepository")
    ProductReviewRepository productReviewRepository;
    
    @Autowired
    @Qualifier("TransactionRepository")
    TransactionRepository transactionRepository;
    
    @Autowired
    @Qualifier("OrderRepository")
    OrderRepository orderRepository;
    
    
    @Autowired
    @Qualifier("Order_ItemRepository")
    Order_ItemRepository orderItemRepository;
    
    @Autowired
    @Qualifier("ModelMapper")
    private ModelMapper modelMaper;
    
    private static final Logger auditTechLog = Logger.getLogger(ProductServices.class);
    
    
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public UserDto addUserRole(User user) {
        User u = userRepository.findByUsername(user.getUsername()).get();
        Set<Role> listRole = user.getRoles();
        u.setRoles(listRole);
        auditTechLog.info("Update User: "+ u.getRoles() + "to "+ user.getRoles());
        userRepository.save(u);
        return modelMaper.map(userRepository.findByUsername(user.getUsername()).get(), UserDto.class);
    }
    
    public List<UserDto> listUser() {
        List<User> list = userRepository.findAll();
        List<UserDto> listDTO = new ArrayList<UserDto>();
        for (User u:list) {
            listDTO.add(modelMaper.map(u, UserDto.class));
        }
        return listDTO;
    }

    public void enableUser(User user) {
        user = userRepository.findByUsername(user.getUsername()).get();
        user.setStatus(true);
        auditTechLog.info("Enable User: "+ user.getUsername());
        userRepository.save(user);
    }
    public void disableUser(User user) {
        user = userRepository.findByUsername(user.getUsername()).get();
        user.setStatus(false);
        auditTechLog.info("Disable User: "+ user.getUsername());
        userRepository.save(user);
    }
    public void addUser(User user) {
        auditTechLog.info("Add User: "+ user.getUsername());
        signUpUser(user);
    }
    public void signUpUser(User user) {
        try {
            if (!userRepository.existsByUsername(user.getUsername())) {
                String passwordEncode = encoder.encode(user.getPassword());
                user.setPassword(passwordEncode);
                userRepository.save(user);
                user = userRepository.findByUsername(user.getUsername()).get();
                Cart cart = new Cart();
                cart.setUser(user);
                user.setCart(cart);
                cartRepository.save(cart);
                auditTechLog.info("Add cart");
                userRepository.save(user);
                auditTechLog.info("Add User: "+ user.getUsername());
            } else {
                System.out.println("Người dùng tồn tại!");
                throw new UserException();
            }
        }catch (UserException e){
            System.out.println(e.userExistsException());
        }
    }
    @Transactional(rollbackFor = {Exception.class, Throwable.class})
    public void deleteUser(User user) {
        Long id = user.getId();
        if(userRepository.existsById(user.getId())){
            auditTechLog.info("Delete User: "+ user.getUsername());
            userRepository.deleteById(id);
        }else{
            System.out.println("Người cần xóa không tồn tại!");
        }
    }
    public User getUser(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        // Tìm lên người dùng và sản phẩm cần thao tac
        User user = userRepository.findByUsername(username).get();
        System.out.println("Người dùng thực hiện thao tác này là: " + user.getUsername());
        return user;
    }
//    public User parseJwt(HttpHeaders headers) {
//        String headerAuth = headers.getFirst("Authorization");
//        if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
//            String jwt = headerAuth.substring(7, headerAuth.length());
//
//            if (jwt != null && jwtUtils.validateJwtToken(jwt)) {
//                String username = jwtUtils.getUserNameFromJwtToken(jwt);
//                return userRepository.findByUsername(username).get();
//            }
//
//        }
//        return null;
//    }
}
