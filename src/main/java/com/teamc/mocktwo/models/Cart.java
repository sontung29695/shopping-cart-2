package com.teamc.mocktwo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.teamc.mocktwo.security.services.AuditListener;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@EntityListeners(AuditListener.class)
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Table(name = "carts")
public class Cart implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "cart_id")
    @TableGenerator(name =  "cart_id", table = "_sequences", pkColumnValue = "cart_id", allocationSize = 1)
    @Column(name = "cart_id", nullable = false)
    private Long cartId;
    
    @Column(name = "price_item_enable", nullable = false)
    private Long priceItemEnable = Long.valueOf(0);
    
    @OneToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "user_id", unique = true)
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private User user;
    
//    @ManyToOne(fetch = FetchType.LAZY, optional=false)
//    @JoinColumn(name = "user_id", nullable = false)
//    private User user;
    
    @OneToMany(mappedBy = "cart", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<Cart_Item> cartItem = new LinkedHashSet<>();
    
    public Cart(Long cart_id) {
        this.cartId = cart_id;
    }
    
    public void addCartItem(Cart_Item cart_item){
        this.cartItem.add(cart_item);
    }
    public void deleteCartItem(Cart_Item cart_item){
        this.cartItem.remove(cart_item.getCartItemId());
    }
}