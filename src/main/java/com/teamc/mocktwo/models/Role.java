package com.teamc.mocktwo.models;

import com.teamc.mocktwo.security.services.AuditListener;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "roles")
public class Role implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "role_id")
    @TableGenerator(name =  "role_id", table = "_sequences", pkColumnValue = "role_id", allocationSize = 1)
    @Column(name = "role_id", nullable = false)
    private Long id;
    
    @Column(name = "role_name", nullable = false, length = 45, unique = true)
    private String name;
    
    public Role(String name) {
        this.name = name;
    }
    
}