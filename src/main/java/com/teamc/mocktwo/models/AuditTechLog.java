package com.teamc.mocktwo.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "audit_tech_log")
public class AuditTechLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long auditLogId;
    
    @Column(name = "level", length = 10)
    private String level;
    
    @Column(name = "message",  length = 1000)
    private String message;
    
    @Column(name = "method_name",  length = 255)
    private String method_name;
    
    @Column(name = "date_time")
    private Instant datetime;
}
