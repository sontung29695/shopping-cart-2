package com.teamc.mocktwo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.teamc.mocktwo.security.services.AuditListener;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@EntityListeners(AuditListener.class)
@NoArgsConstructor
@Table(name = "transactions")
public class Transaction implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "transaction_id")
    @TableGenerator(name =  "transaction_id", table = "_sequences", pkColumnValue = "transaction_id", allocationSize = 1)
    @Column(name = "transaction_id", nullable = false)
    private Long id;
    
    @Column(name = "code", nullable = false)
    private String code;
    
    @Column(name = "amount_total", nullable = false)
    private Long amountTotal;
    
    @ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private User user;

    @ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "order_id")
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Order order;
    
    public Transaction(String code, Long amountTotal, User user, Order order) {
        this.code = code;
        this.amountTotal = amountTotal;
        this.user = user;
        this.order = order;
    }
}