package com.teamc.mocktwo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.teamc.mocktwo.security.services.AuditListener;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@EntityListeners(AuditListener.class)
@NoArgsConstructor
@Table(name = "cart_item")
public class Cart_Item implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "cart_item_id")
    @TableGenerator(name =  "cart_item_id", table = "_sequences", pkColumnValue = "cart_item_id", allocationSize = 1)
    @Column(name = "cart_item_id", nullable = false)
    private Long cartItemId;
    
    @ManyToOne(fetch = FetchType.EAGER, optional=true)
    @JoinColumn(name = "cart_id", nullable = false)
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Cart cart;
    
    @ManyToOne(fetch = FetchType.EAGER, optional=true)
    @JoinColumn(name = "product_id", nullable = false)
//    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Product product;

    @Column(name = "quantity_product", nullable = false)
    private Long quantityProduct;
    
    @Column(name = "price_total", nullable = false)
    private Long priceTotal = Long.valueOf(0);
    
    @Column(name = "enable", nullable = false)
    private Boolean enable = false;
    
    public Cart_Item(Cart cart, Product product, Long quantity_product, Long price_total) {
        this.cart = cart;
        this.product = product;
        this.quantityProduct = quantity_product;
        this.priceTotal = price_total;
    }
}
