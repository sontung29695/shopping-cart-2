package com.teamc.mocktwo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.teamc.mocktwo.security.services.AuditListener;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "images")
public class Image implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "image_id")
    @TableGenerator(name =  "image_id", table = "_sequences", pkColumnValue = "image_id", allocationSize = 1)
    @Column(name = "image_id", nullable = false)
    private Long imageId;
    
    @Lob
    byte[] content;
    
    @Column(name = "imageName", nullable = false)
    String name;
    
    @ManyToOne(fetch = FetchType.LAZY, optional=true)
    @JoinColumn(name = "user_id")
//    @JsonIgnore
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private User user;
}
