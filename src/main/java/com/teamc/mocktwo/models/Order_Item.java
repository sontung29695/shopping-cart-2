package com.teamc.mocktwo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.teamc.mocktwo.security.services.AuditListener;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@EntityListeners(AuditListener.class)
@NoArgsConstructor
@Table(name = "order_item")
public class Order_Item implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "order_item_id")
    @TableGenerator(name =  "order_item_id", table = "_sequences", pkColumnValue = "order_item_id", allocationSize = 1)
    @Column(name = "order_item_id", nullable = false)
    private Long orderItemId;
    
    @ManyToOne(fetch = FetchType.EAGER, optional=true)
    @JoinColumn(name = "order_id", nullable = false)
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Order order;

    @ManyToOne(fetch = FetchType.EAGER, optional=true)
    @JoinColumn(name = "product_id", nullable = false)
//    @JsonIgnore
//    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Product product;

    @Column(name = "quantity_product", nullable = false)
    private Long quantityProduct;
    
    public Order_Item(Order order, Product product, Long quantity_product) {
        this.order = order;
        this.product = product;
        this.quantityProduct = quantity_product;
    }
}
