package com.teamc.mocktwo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.teamc.mocktwo.security.services.AuditListener;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@EntityListeners(AuditListener.class)
@NoArgsConstructor
@Table(name = "product_review")
public class ProductReview implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "review_id")
    @TableGenerator(name =  "review_id", table = "_sequences", pkColumnValue = "review_id", allocationSize = 1)
    @Column(name = "review_id", nullable = false)
    private Long id;
    
    @ManyToOne(fetch = FetchType.EAGER, optional=true)
    @JoinColumn(name = "user_id", nullable = false)
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private User user;
    
    @ManyToOne(fetch = FetchType.EAGER, optional=true)
    @JoinColumn(name = "product_id", nullable = false)
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Product product;
    
    @Column(name = "product_review", nullable = false, length = 150)
    private String productReview;
    
    public ProductReview(User user, Product product, String productReview) {
        this.user = user;
        this.product = product;
        this.productReview = productReview;
    }
}