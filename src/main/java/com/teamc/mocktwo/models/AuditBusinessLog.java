package com.teamc.mocktwo.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "audit_business_log")
public class AuditBusinessLog implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "audit_log_id")
    @TableGenerator(name =  "audit_log_id", table = "_sequences", pkColumnValue = "audit_log_id", allocationSize = 1)
    @Column(name = "audit_log_id", nullable = false)
    private Long auditLogId;
    
    @Column(name = "event", nullable = false, length = 200)
    private String event;
    
    @Column(name = "description", nullable = false, length = 1000)
    private String description;
    
//    @Column(name = "tech_log", nullable = false)
//    private Boolean isTechlog = false;
//
//
//    @Column(name = "business_log", nullable = false)
//    private Boolean isBusinesslog = false;
    
    @Column(name = "createdAt", nullable = false)
    // @Temporal(TemporalType.DATE)
    private Date createdAt = new Date();
    
//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "user_id", nullable = false)
//    @JsonIgnore
//    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) // khi dùng fetch Lazy
//    private User user;
    
    public AuditBusinessLog(String event, String description) {
        this.event = event;
        this.description = description;
//        this.user = user;
    }
}