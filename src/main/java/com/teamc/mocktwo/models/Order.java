package com.teamc.mocktwo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.teamc.mocktwo.security.services.AuditListener;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@EntityListeners(AuditListener.class)
@NoArgsConstructor
@Table(name = "orders")
public class Order implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "order_id")
    @TableGenerator(name =  "order_id", table = "_sequences", pkColumnValue = "order_id", allocationSize = 1)
    @Column(name = "order_id", nullable = false)
    private Long orderId;
    
    @ManyToOne(fetch = FetchType.EAGER, optional=true)
    @JoinColumn(name = "user_id", nullable = false)
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private User user;
    
    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch=FetchType.EAGER, orphanRemoval=true)
    private Set<Order_Item> order_items = new LinkedHashSet<Order_Item>();
    
    @Column(name = "amount_total", nullable = false)
    private Long amountTotal;
    
    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch=FetchType.EAGER, orphanRemoval=true)
    private Set<Transaction> transactions = new LinkedHashSet<Transaction>();
    
    public Order(User user) {
        this.user = user;
        this.setAmountTotal((long)0);
    }
    public void addOrderItem(Order_Item orderItem){
        this.order_items.add(orderItem);
    }
    public void removeOrderItem(Order_Item orderItem){
        this.order_items.remove(orderItem);
    }
}