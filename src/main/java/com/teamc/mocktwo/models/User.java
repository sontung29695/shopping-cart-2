package com.teamc.mocktwo.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.teamc.mocktwo.security.services.AuditListener;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Entity
@EntityListeners(AuditListener.class)
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Table(name = "users")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "user_id")
    @TableGenerator(name =  "user_id", table = "_sequences", pkColumnValue = "user_id", allocationSize = 1)
    @Column(name = "user_id", nullable = false)
    private Long id;
    
    @Column(name = "username", nullable = false, length = 45, unique = true)
    private String username;
    
    @Column(name = "password", nullable = false, length = 120)
    private String password;
    
    @Column(name = "fullname", nullable = false, length = 45)
    private String fullname;
    
    @Column(name = "email", nullable = false, length = 45)
    private String email;
    
    @Column(name = "phone", nullable = false, length = 45)
    private String phone;
    
    @Column(name = "secret", nullable = false, length = 45)
    private String secret;
    
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch=FetchType.EAGER, orphanRemoval=true)
    private Set<ProductReview> productReviews = new LinkedHashSet<ProductReview>();
    
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch=FetchType.LAZY, orphanRemoval=false)
    private Set<Image> images = new LinkedHashSet<Image>();
    
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch=FetchType.EAGER, orphanRemoval=true)
    private Set<Order> orders = new LinkedHashSet<>();
    
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch=FetchType.EAGER, orphanRemoval=true)
    private Set<Transaction> transactions = new LinkedHashSet<Transaction>();
//
//    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch=FetchType.EAGER, orphanRemoval=true)
//    private Set<AuditBusinessLog> auditLogs = new LinkedHashSet<AuditBusinessLog>();
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new LinkedHashSet<Role>();
    
    @OneToOne( mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval=true)
    private Cart cart ;
    
//    @OneToMany(mappedBy = "user")
//    private List<Cart> carts= new ArrayList<Cart>();;
    
    @Column(name = "status", nullable = false)
    private Boolean status = true;
    
    public User(Long id, String username, String password, String fullname, String email, String phone, String secret, Boolean status) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.fullname = fullname;
        this.email = email;
        this.phone = phone;
        this.secret = secret;
        this.status = status;
    }
    public User( String username, String password, String fullname, String email, String phone, String secret, Boolean status) {
        this.username = username;
        this.password = password;
        this.fullname = fullname;
        this.email = email;
        this.phone = phone;
        this.secret = secret;
        this.status = status;
    }
    public void addRole(Role role){
        this.roles.add(role);
    }
    
    public void removeRole(Role role){
        this.roles.remove(role);
    }
    
    public void addOrder(Order order){
        this.orders.add(order);
    }
    
    public void removeOrder(Order order){
        this.orders.remove(order);
    }
    
    public void addProductReview(ProductReview productReview){
        this.productReviews.add(productReview);
    }
    
    public void removeProductReview(ProductReview productReview){
        this.productReviews.remove(productReview);
    }
    
    public void addTransaction(Transaction transaction){
        this.transactions.add(transaction);
    }
    
    public void removeTransaction(Transaction transaction){
        this.transactions.remove(transaction);
    }
//
//    public void addAuditLog(AuditBusinessLog audit_log){
//        this.auditLogs.add(audit_log);
//    }
//
//    public void removeAuditLog(AuditBusinessLog audit_log){
//        this.auditLogs.remove(audit_log);
//    }
    

}