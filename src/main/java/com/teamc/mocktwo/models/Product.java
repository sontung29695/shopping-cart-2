package com.teamc.mocktwo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.teamc.mocktwo.security.services.AuditListener;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@EntityListeners(AuditListener.class)
@NoArgsConstructor
@Table(name = "products")
public class Product implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "product_id")
    @TableGenerator(name =  "product_id", table = "_sequences", pkColumnValue = "product_id", allocationSize = 1)
    @Column(name = "product_id", nullable = false)
    private Long id;
    
    @Column(name = "name", nullable = false, length = 45)
    private String name;
    
    @Column(name = "price", nullable = false)
    private Long price;
    
    @Column(name = "quantity", nullable = false)
    private Long quantity;
    
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, fetch=FetchType.EAGER, orphanRemoval=true)
    private Set<ProductReview> productReviews = new LinkedHashSet<ProductReview>();
    
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, fetch=FetchType.EAGER, orphanRemoval=true)
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Set<Order_Item> order_items = new LinkedHashSet<Order_Item>();
    
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, fetch=FetchType.EAGER, orphanRemoval=true)
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Set<Cart_Item> cart_items = new LinkedHashSet<Cart_Item>();
    
    public Product(Long id, String name, Long price, Long quantity) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }
    public Product(Long id, Long price, Long quantity) {
        this.id = id;
        this.price = price;
        this.quantity = quantity;
    }
    public void addCartItem(Cart_Item cart_item){
        this.cart_items.add(cart_item);
    }
    public void deleteCartItem(Cart_Item cart_item){
        this.cart_items.remove(cart_item.getCartItemId());
    }
}