package com.teamc.mocktwo.repository;

import com.teamc.mocktwo.models.Cart;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@Qualifier("CartRepository")
public interface CartRepository extends JpaRepository<Cart, Long> {
    @Modifying
    @Query(value = "delete from carts b where b.user_id= ?1", nativeQuery = true)
    public void deleteByUser_id(Long user_id);
//    boolean existsByUser_id(Long user_id);
}