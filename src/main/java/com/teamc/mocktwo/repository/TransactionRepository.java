package com.teamc.mocktwo.repository;

import com.teamc.mocktwo.models.Transaction;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@Qualifier("TransactionRepository")
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    @Modifying
    @Query(value = "delete from transactions b where b.user_id= ?1", nativeQuery = true)
    public void deleteByUser_id(Long user_id);
    
    @Modifying
    @Query(value = "delete from transactions b where b.order_id= ?1", nativeQuery = true)
    public void deleteByOrderId(Long order_id);
    
    
    @Modifying
    @Query(value = "UPDATE `shopping_cart_test`.`transactions` SET `order_id` = ?1, `user_id` = ?2 WHERE (`code` = ?3);", nativeQuery = true)
    public void insertUserIdOrderId(Long order_id , Long user_id, String code);
}