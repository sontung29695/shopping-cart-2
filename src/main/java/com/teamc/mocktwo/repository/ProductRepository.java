package com.teamc.mocktwo.repository;


import com.teamc.mocktwo.models.Product;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Qualifier("ProductRepository")
public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findAll();
    boolean existsByName(String name);
}