package com.teamc.mocktwo.repository;

import com.teamc.mocktwo.models.AuditBusinessLog;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
@Qualifier("Audit_LogRepository")
public interface Audit_LogRepository extends JpaRepository<AuditBusinessLog, Long> {
    @Modifying
    @Query(value = "delete from audit_log b where b.user_id= ?1", nativeQuery = true)
    public void deleteByUser_id(Long user_id);
//    boolean existsByUser_id(Long user_id);
}