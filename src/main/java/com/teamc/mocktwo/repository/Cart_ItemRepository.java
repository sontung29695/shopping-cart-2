package com.teamc.mocktwo.repository;

import com.teamc.mocktwo.models.Cart_Item;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@Qualifier("Cart_ItemRepository")
public interface Cart_ItemRepository extends JpaRepository<Cart_Item, Long> {
    
//    @Modifying
//    @Query(value = "select * from cart_item b where b.product_id= ?1", nativeQuery = true)
    public Cart_Item findByProduct_id(Long product_id);
    
    @Modifying
    @Query(value = "select * from cart_item b where b.enable= 1", nativeQuery = true)
    public Iterable<Cart_Item> findByEnable();
    
    @Modifying
    @Query(value = "delete from cart_item b where b.enable= 1", nativeQuery = true)
    public void deleteByEnable();
    
    @Modifying
    @Query(value = "delete from cart_item b where b.product_id= ?1", nativeQuery = true)
    public void deleteByProduct_id(Long product_id);
    
    @Modifying
    @Query(value = "delete from cart_item b where b.cart_id= ?1", nativeQuery = true)
    public void deleteByCart_id(Long cart_id);
//    boolean existsByCart_id(Long cart_id);
//    boolean existsByProduct_id(Long product_id);
}