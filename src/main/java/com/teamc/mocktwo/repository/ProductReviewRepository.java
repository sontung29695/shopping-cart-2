package com.teamc.mocktwo.repository;


import com.teamc.mocktwo.models.ProductReview;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@Qualifier("ProductReviewRepository")
public interface ProductReviewRepository extends JpaRepository<ProductReview, Long> {
    @Modifying
    @Query(value = "delete from product_review b where b.user_id= ?1", nativeQuery = true)
    public void deleteByUser_id(Long user_id);
    
    @Modifying
    @Query(value = "delete from product_review b where b.product_id= ?1", nativeQuery = true)
    public void deleteByProduct_id(Long product_id);
    
    @Override
    @Modifying
    @Query(value = "delete from product_review b where b.review_id= ?1", nativeQuery = true)
    public void deleteById(Long review_id);
//    boolean existsByUser_id(Long user_id);
}