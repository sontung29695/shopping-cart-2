package com.teamc.mocktwo.repository;


import com.teamc.mocktwo.models.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@Qualifier("UserRepository")
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
    List<User> findAll();
    boolean existsByUsername(String username);
    void deleteByUsername(String deleteByUsername);
    @Modifying
    @Query(value = "delete from users b where b.user_id= ?1", nativeQuery = true)
    public void deleteByUser_id(Long user_id);
    
    
}