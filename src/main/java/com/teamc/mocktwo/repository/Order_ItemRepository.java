package com.teamc.mocktwo.repository;

import com.teamc.mocktwo.models.Cart_Item;
import com.teamc.mocktwo.models.Order_Item;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@Qualifier("Order_ItemRepository")
public interface Order_ItemRepository extends JpaRepository<Order_Item, Long> {
    @Modifying
    @Query(value = "delete from order_item b where b.order_id= ?1", nativeQuery = true)
    public void deleteByOrderId(Long order_id);
    
    @Modifying
    @Query(value = "delete from order_item b where b.product_id= ?1", nativeQuery = true)
    public void deleteByProduct_id(Long product_id);
    
//    boolean existsByOrder_id(Long order_id);
//
//    boolean existsByProduct_id(Long product_id);
}