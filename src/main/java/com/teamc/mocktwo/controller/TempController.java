package com.teamc.mocktwo.controller;

import com.teamc.mocktwo.dto.ProductDto;
import com.teamc.mocktwo.dto.UserDto;

import com.teamc.mocktwo.models.Image;
import com.teamc.mocktwo.models.Order;
import com.teamc.mocktwo.models.Product;
import com.teamc.mocktwo.models.User;
import com.teamc.mocktwo.payload.request.ProductReviewRequest;
import com.teamc.mocktwo.payload.request.TransactionRequest;
import com.teamc.mocktwo.security.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RestController
public class TempController {
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(){
        return "Helloworld";
    }
    @RequestMapping(value = "/api/admin", method = RequestMethod.GET)
    public String homeAdmin(){
        return "Hello Admin";
    }
    @RequestMapping(value = "/api/user", method = RequestMethod.GET)
    public String homeUser(){
        return "Hello User";
    }
    @RequestMapping(value = "/api/admin/test", method = RequestMethod.GET)
    public String homeAdminTest(){
        return "Hello Admin Test";
    }
    @RequestMapping(value = "/api/user/test", method = RequestMethod.GET)
    public String homeUserTest(){
        return "Hello User Test";
    }
    @RequestMapping(value = "/api/user/test/test", method = RequestMethod.GET)
    public String homeUserTest2(){
        return "Hello User Test1";
    }
    
    @Autowired
    private UserServices userServices;
    
    @Autowired
    private ProductServices productServices;
    
    @Autowired
    private CartServices cartServices;
    
    @Autowired
    private OrderServices orderServices;
    
    @Autowired
    private ProductReviewServices productReviewServices;
    
    @Autowired
    private TransactionServices transactionServices;
    
    @Autowired
    private ImageServices imageServices;
    
    @RequestMapping(value = "/api/login", method = RequestMethod.GET)
    public String customLogin(){
        return "customLogin";
    }
    
    @PostMapping("/api/admin/add-user")
    public ResponseEntity<?> addUser(@Valid @RequestBody User user){
        userServices.addUser(user);
        List<UserDto> list = userServices.listUser();
        return ResponseEntity.ok(list);
    }
    @GetMapping("/api/admin/list-user")
    public ResponseEntity<?> listUser(){
        List<UserDto> list = userServices.listUser();
        return ResponseEntity.ok(list);
    }
    @PostMapping("/api/signup")
    public ResponseEntity<?> signUp(@Valid @RequestBody User user){
        userServices.signUpUser(user);
        List<UserDto> list = userServices.listUser();
        return ResponseEntity.ok(list);
    }
    
    @PostMapping("/api/admin/delete-user")
    public ResponseEntity<?> deteteUser(@Valid @RequestBody User user){
        userServices.deleteUser(user);
        List<UserDto> list = userServices.listUser();
        return ResponseEntity.ok(list);
    }
    
    @PostMapping("/api/admin/add-role-user")
    public ResponseEntity<?> addUserRole(@Valid @RequestBody User user){
        UserDto userDto = userServices.addUserRole(user);
        return ResponseEntity.ok(userDto);
    }
    @PostMapping("/api/list-product")
    public ResponseEntity<?> listProduct(){
        List<ProductDto> listDTO = productServices.listProducts();
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/admin/add-product")
    public ResponseEntity<?> addProduct(@Valid @RequestBody Product product){
        productServices.addProduct(product);
        List<ProductDto> listDTO = productServices.listProducts();
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/admin/update-product")
    public ResponseEntity<?> updateProduct(@Valid @RequestBody Product product){
        productServices.updateProduct(product);
        List<ProductDto> listDTO = productServices.listProducts();
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/admin/delete-product")
    public ResponseEntity<?> deleteProduct(@Valid @RequestBody Product product){
        productServices.deleteProduct(product);
        List<ProductDto> listDTO = productServices.listProducts();
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/user/add-to-cart")
    public ResponseEntity<?> addToCart(@Valid @RequestBody Product product, @RequestHeader HttpHeaders headers){
        //User Jwt = userServices.parseJwt(headers);
        cartServices.addToCart(product);
        List<UserDto> listDTO = userServices.listUser();
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/user/delete-from-cart")
    public ResponseEntity<?> deleteFromCart(@Valid @RequestBody Product product){
        cartServices.deleteFromCart(product);
        List<UserDto> listDTO = userServices.listUser();
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/user/select-product-in-cart")
    public ResponseEntity<?> selectFromCart(@Valid @RequestBody Product product){
        // Nếu tích chọn (là lúc enable) không phải để mua mà là để xóa thì yêu cầu frontend trước khi gửi lệnh xóa thì gửi thêm lệnh disable
        cartServices.enableProductInCart(product);
        List<UserDto> listDTO = userServices.listUser();
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/user/unselect-product-in-cart")
    public ResponseEntity<?> unselectFromCart(@Valid @RequestBody Product product){
        cartServices.disableProductInCart(product);
        List<UserDto> listDTO = userServices.listUser();
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/user/add-order")
    public ResponseEntity<?> addOrder(){
        orderServices.addOrder();
        List<UserDto> listDTO = userServices.listUser();
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/admin/delete-order")
    public ResponseEntity<?> deleteOrder(@Valid @RequestBody Order order){
        orderServices.deleteOrder(order);
        List<UserDto> listDTO = userServices.listUser();
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/user/rollback-order")
    public ResponseEntity<?> rollBackOrder(@Valid @RequestBody Order order) throws InterruptedException {
        orderServices.rollBackCart(order);
        orderServices.deleteOrder(order);
        List<UserDto> listDTO = userServices.listUser();
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/user/add-transaction")
    public ResponseEntity<?> addTransaction(@RequestBody TransactionRequest transactionRequest) {
        String code =  String.valueOf(transactionRequest.getCode());
        Order order = transactionRequest.getOrder();
        transactionServices.addTransaction(order ,code);
        List<UserDto> listDTO = userServices.listUser();
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/user/add-review")
    public ResponseEntity<?> addReview(@RequestBody ProductReviewRequest productReviewRequest) {
        productReviewServices.addReview(productReviewRequest.getProductId(), productReviewRequest.getProductReview());
        List<ProductDto> listDTO = productServices.listProducts();
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/user/delete-review")
    public ResponseEntity<?> deleteReview(@RequestBody ProductReviewRequest productReviewRequest) {
        productReviewServices.deleteReview(productReviewRequest.getDeleteProductReviewId());
        List<ProductDto> listDTO = productServices.listProducts();
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/user/upload-image")
    public ResponseEntity<?> uploadImage(@RequestParam("file")  MultipartFile multipartImage) throws Exception {
        imageServices.uploadImage(multipartImage);
        List<UserDto> listDTO = userServices.listUser();
        return ResponseEntity.ok(listDTO);
    }
    @GetMapping(value = "/api/user/download-image/{imageId}", produces = MediaType.IMAGE_JPEG_VALUE)
    public Resource downloadImage(@PathVariable Long imageId)  {
        Resource resource = imageServices.downloadImage(imageId);
        return resource;
    }
    @PostMapping("/api/user/upload-image-system")
    public ResponseEntity<?> uploadImageSys(@RequestParam("file")  MultipartFile multipartImage) throws Exception {
        imageServices.uploadImageSys(multipartImage);
        List<UserDto> listDTO = userServices.listUser();
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/user/delete-image-sys")
    public ResponseEntity<?> deleteImageSys(@RequestBody String path) throws Exception {
        imageServices.deleteImageSys(path);
        List<UserDto> listDTO = userServices.listUser();
        return ResponseEntity.ok(listDTO);
    }
    @PostMapping("/api/user/delete-image")
    public ResponseEntity<?> deleteImage(@RequestBody Image image) throws Exception {
        imageServices.deleteImage(image);
        List<UserDto> listDTO = userServices.listUser();
        return ResponseEntity.ok(listDTO);
    }
}
