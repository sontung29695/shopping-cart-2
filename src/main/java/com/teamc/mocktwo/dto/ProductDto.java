package com.teamc.mocktwo.dto;

import com.teamc.mocktwo.models.Cart_Item;
import com.teamc.mocktwo.models.Order_Item;
import com.teamc.mocktwo.models.ProductReview;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class ProductDto {
    private Long id;
    private String name;
    private Long price;
    private Long quantity;
    private List<ProductReview> productReviews;
    private List<Order_Item> order_items = new ArrayList<Order_Item>();
    private List<Cart_Item> cart_items = new ArrayList<Cart_Item>();
}
