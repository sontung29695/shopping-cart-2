package com.teamc.mocktwo.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class OrderDto implements Serializable {
    private final Long order_id;
    private final Long amount_total;
}
