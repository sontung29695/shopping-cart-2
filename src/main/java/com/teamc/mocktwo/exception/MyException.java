package com.teamc.mocktwo.exception;

import lombok.Data;

@Data
public class MyException extends RuntimeException{
    private String message;
    

    public void showMessage(){
        System.out.println(this.message);
    }
    public MyException(String mes){
        this.message = mes;
    }
    
}
