package com.teamc.mocktwo.payload.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class ProductReviewRequest {
    @NotBlank
    private Long productId;
    
    @NotBlank
    private String productReview;
    
    @NotBlank
    private Long deleteProductReviewId;
    
}