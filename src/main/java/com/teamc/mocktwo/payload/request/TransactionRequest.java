package com.teamc.mocktwo.payload.request;

import com.teamc.mocktwo.models.Order;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public class TransactionRequest {
    @NotBlank
    private Order order;
    
    @NotBlank
    private String code;
}
