package com.teamc.mocktwo;

import com.teamc.mocktwo.models.*;
import com.teamc.mocktwo.repository.CartRepository;
import com.teamc.mocktwo.repository.ProductRepository;
import com.teamc.mocktwo.repository.RoleRepository;
import com.teamc.mocktwo.repository.UserRepository;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class DemoApplication {
	private static final Logger auditTechLog = Logger.getLogger(DemoApplication.class);
	public static void main(String[] args) {
		ApplicationContext context =  SpringApplication.run(DemoApplication.class, args);
		
		UserRepository userRepository = context.getBean(UserRepository.class);
		RoleRepository roleRepository = context.getBean(RoleRepository.class);
		ProductRepository productRepository = context.getBean(ProductRepository.class);
		CartRepository cartRepository = context.getBean(CartRepository.class);
		PasswordEncoder encoder = context.getBean(PasswordEncoder.class);
		
		User user1 = new User(Long.valueOf(1), "admin", encoder.encode("admin"),"Admin", "admin@adnib.com", "0987654321", "secret1", true);
		User user2 = new User( "user", encoder.encode("admin"),"admin", "user@user.com", "0987654322", "secret2", true);
		User user3 = new User("mod", encoder.encode("admin"),"admin", "mod@mod.com", "0987654323", "secret3", true);
		
		Product product1 = new Product(Long.valueOf(1), "TV" , Long.valueOf(1000), Long.valueOf(10));
		Product product2 = new Product(Long.valueOf(2), "Tu lanh" , Long.valueOf(2000), Long.valueOf(10));
		Product product3 = new Product(Long.valueOf(3), "May giat" , Long.valueOf(3000), Long.valueOf(10));
		Product product4 = new Product(Long.valueOf(4), "Smartphone" , Long.valueOf(4000), Long.valueOf(10));
		Product product5 = new Product(Long.valueOf(5), "Computer" , Long.valueOf(5000), Long.valueOf(10));
		
		if(!productRepository.existsByName(product1.getName())){
			productRepository.save(product1);
		}
		if(!productRepository.existsByName(product2.getName())){
			productRepository.save(product2);
		}
		if(!productRepository.existsByName(product3.getName())){
			productRepository.save(product3);
		}
		if(!productRepository.existsByName(product4.getName())){
			productRepository.save(product4);
		}
		if(!productRepository.existsByName(product5.getName())){
			productRepository.save(product5);
		}
		
//		Luôn thêm trước các ROLE vào trong database -------------------------------------------------------------
		
		if(!roleRepository.existsByName("ROLE_ADMIN")){
			roleRepository.save(new Role("ROLE_ADMIN"));
		}
		if(!roleRepository.existsByName("ROLE_USER")){
			roleRepository.save(new Role("ROLE_USER"));
		}
		if(!roleRepository.existsByName("ROLE_MOD")){
			roleRepository.save(new Role("ROLE_MOD"));
		}
		
		
//		--------------------------------------------------------------------------------------------------------
		
		Role admin_role = roleRepository.findByName("ROLE_ADMIN");
		Role user_role = roleRepository.findByName("ROLE_USER");
		Role mod_role = roleRepository.findByName("ROLE_MOD");
		
		user1.addRole(admin_role);
		user1.addRole(user_role);
		user1.addRole(mod_role);
		user2.addRole(user_role);
		user2.addRole(mod_role);
		user3.addRole(user_role);
		
		if(!userRepository.existsByUsername(user1.getUsername())){
			userRepository.save(user1);
			Cart cart = new Cart();
			User user_temp = userRepository.findByUsername(user1.getUsername()).get();
			user_temp.setCart(cart);
			cart.setUser(user_temp);
			cartRepository.save(cart);
			userRepository.save(user_temp);
		}
		if(!userRepository.existsByUsername(user2.getUsername())){
			userRepository.save(user2);
			Cart cart = new Cart();
			User user_temp = userRepository.findByUsername(user2.getUsername()).get();
			user_temp.setCart(cart);
			cart.setUser(user_temp);
			cartRepository.save(cart);
			userRepository.save(user_temp);
		}
		if(!userRepository.existsByUsername(user3.getUsername())){
			userRepository.save(user3);
			Cart cart = new Cart();
			User user_temp = userRepository.findByUsername(user3.getUsername()).get();
			user_temp.setCart(cart);
			cart.setUser(user_temp);
			cartRepository.save(cart);
			userRepository.save(user_temp);
		}
		
	}

}
